<?php

function drulenium_imgur_settings_form($form, &$form_state) {
  $form['drulenium_imgur_client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('imgur client_id'),
      '#description' => t('your imgur app client id.'),
      '#default_value' => variable_get('drulenium_imgur_client_id', ''),
      '#required' => TRUE,
  );
  $form['drulenium_imgur_client_secret'] = array(
      '#type' => 'password',
      '#title' => t('imgur client_secret'),
      '#description' => t('your imgur app client secret.<br/> Secret Key is stored in plain text in the database.'),
      '#default_value' => variable_get('drulenium_imgur_client_secret', ''),
      '#required' => TRUE,
  );

  $form = system_settings_form($form);
  return $form;
}
