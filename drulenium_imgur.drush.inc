<?php

/**
 * @file
 * Download imgur dependencies via drush framework.
 */

/**
 * Implements hook_drush_command().
 */
function drulenium_imgur_drush_command() {
  $items['imgur-download-client'] = array(
    'callback'    => 'drush_drulenium_imgur_download_client',
    'description' => dt('This command used to download imgur php api client.'),
    'aliases'     => array('imgur-dl'),
  );
  return $items;
}

/**
 * Helper function downloads imgur php api client.
 */
function drush_drulenium_imgur_download_client() {

  // Get the path from the argument, if site, or use the default.
  $drush_context = drush_get_context('DRUSH_DRUPAL_ROOT');

  // Can we use Libraries API?
  if (module_exists('libraries') && function_exists('libraries_get_path')) {
    $libraries_dir = libraries_get_path('php-imgur-api-client');
  }

  // Load the path.
  $path = $libraries_dir ? $drush_context . '/' . $libraries_dir : $drush_context . '/sites/all/libraries/php-imgur-api-client';

  drush_log(dt('Download destination path: "@path"', array('@path' => $path)), 'notice');

  // Set the directory to the download location.
  $olddir = getcwd();

  $download_path = str_replace('/php-imgur-api-client', '', $path);

  chdir($download_path);

  $download_uri = 'http://drulenium.org/downloads/php-imgur-api-client.zip';
  // Download the zip archive.
  if ($filepath = drush_download_file($download_uri, $download_path . '/php-imgur-api-client.zip')) {
    $filename = basename($filepath);
    $dirname = basename($filepath, '.zip');

    // Decompress the zip archive.
    drush_tarball_extract($filename);

    // Change the directory name to "select2" if needed.
    if ($dirname != 'php-imgur-api-client') {
      drush_move_dir($dirname, 'php-imgur-api-client', TRUE);
      $dirname = 'php-imgur-api-client';
    }
  }

  if (is_dir($dirname)) {
    drush_log(dt('php-imgur-api-client has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the php-imgur-api-client to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);

}
